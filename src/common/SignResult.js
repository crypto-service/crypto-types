class SignResult {

    constructor(errors) {
        this.signErrors = errors;
    }

    /**
     * Признак наличия ошибок
     * @return boolean
     */
    get isError(){
        return this.signErrors.length > 0;
    }

    /**
     * Список ошибок
     * @return Array.<CryptoError>
     */
    get errors(){
        return this.signErrors;
    }
}
export {SignResult};