class InitResult {

    constructor(errors = [], information = {}) {
        this.initErrors = errors;
        this.initInformation = information;
    }

    /**
     * Признак наличия ошибок
     * @return boolean
     */
    get isError(){
        return this.initErrors.length > 0;
    }

    /**
     * Список ошибок
     * @return Array.<CryptoError>
     */
    get errors(){
        return this.initErrors;
    }

    /**
     * Информация о пакете
     * @return CryptoPackage
     */
    get information() {
        return this.initInformation;
    }
}
export {InitResult};