class CryptoPackage {

    constructor(id = '', properties = {}) {
        this.packageId = id;
        this.packageProperties = properties;
    }
    /**
     * Идентификатор пакета
     * @return string
     */
    get id(){
        return this.packageId;
    }

    /**
     * Свойства пакета, переданные ранее через API
     * @return object
     */
    get properties(){
        return this.packageProperties;
    }
}

export { CryptoPackage }