class CryptoError {

    constructor(code = '', message = '', description = '') {
        this.errorCode = code;
        this.errorMessage = message;
        this.errorDescription = description;
    }
    /**
     * Код ошибки
     * @return string
     */
    get code(){
        return this.errorCode;
    };

    /**
     * Сообщение
     * @return string
     */
    get message(){
        return this.errorMessage;
    };

    /**
     * Описание ошибки (возможные действия по устранению)
     * @return string
     */
    get description(){
        return this.errorDescription;
    };
}
export { CryptoError }