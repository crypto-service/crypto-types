class CryptoCertificate {

    constructor({fio, position, inn, organization, notAfter, notBefore, serial, thumbprint, valid}) {
        this.fio = fio;
        this.position = position;
        this.inn = inn;
        this.organization = organization;
        this.notAfter = notAfter;
        this.notBefore = notBefore;
        this.serial = serial;
        this.thumbprint = thumbprint;
        this.valid = valid;
    }
    /**
     * ФИО подписанта
     * @return string
     */
    get getFio(){
        return this.fio;
    }

    /**
     * Должность подписанта
     * @return string
     */
    get getPosition(){
        return this.position;
    }

    /**
     * ИНН организации
     * @return string
     */
    get getInn(){
        return this.inn;
    }

    /**
     * Навание организации
     * @return string
     */
    get getOrganization(){
        return this.organization;
    }

    /**
     * Начало действия сертификата
     * @return Date
     */
    get getNotAfter(){
        return this.notAfter;
    }

    /**
     * Окончание действия сертификата
     * @return Date
     */
    get getNotBefore(){
        return this.notBefore;
    }

    /**
     * Серийный номер сертификата
     * @return string
     */
    get getSerial(){
        return this.serial;
    }

    /**
     * Отпечаток сертификата
     * @return string
     */
    get getThumbprint(){
        return this.thumbprint;
    }

    /**
     * Признак валидности сертификата
     * @return boolean
     */
    get getIsValid(){
        return this.valid;
    }
}
export { CryptoCertificate }