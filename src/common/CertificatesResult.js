class CertificatesResult {

    /**
     * @param {CryptoError[]} errors - Список ошибок
     * @param {CryptoCertificate[]} certificates - Список ошибок
     * @return boolean
     */
    constructor(errors, certificates) {
        this.errorList = errors;
        this.certificateList = certificates;
    }

    /**
     * Признак наличия ошибок
     * @return boolean
     */
    get isError(){
        return this.errorList.length > 0;
    }

    /**
     * Список ошибок
     * @return Array.<CryptoError>
     */
    get errors(){
        return this.errorList;
    }

    /**
     * Список сертификатов
     * @return Array.<CryptoCertificate>
     */
    get certificates(){
        return this.certificateList;
    }
}

export { CertificatesResult }